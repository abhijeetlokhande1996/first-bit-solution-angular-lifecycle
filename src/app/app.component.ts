import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'AngularLifeCycle';
  content: string = 'I Love My India!!!';
  displayTestComponent: boolean = true;

  @ViewChild('appLogin') appLoginComponent: LoginComponent;
  constructor() {
    console.log('App constructor');
  }

  ngOnInit() {
    console.log('App init');
  }
  ngAfterViewInit() {
    console.log('App afterViewInit');
    // console.log(this.appLoginComponent);
    this.appLoginComponent.username = 'abc@xyz.com';
    this.appLoginComponent.password = 'abcd';
    console.log('Print Execution from App');
    this.appLoginComponent.print();
  }

  toggleTestComponent() {
    this.displayTestComponent = !this.displayTestComponent;
  }
}
