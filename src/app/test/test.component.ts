import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
})
export class TestComponent implements OnInit, OnDestroy {
  name: string;
  arr: Array<number> = [];

  @Input('content') content: string;

  constructor() {
    console.warn('Constructor - Test');
    this.arr = [1, 2, 3, 4];
    console.log('Content - Constructor', this.content);
  }

  ngOnInit(): void {
    console.warn('ngOnInit - Test');
    console.log(this.arr);
    console.log('Content - ngOnInit', this.content);
  }
  ngOnDestroy() {
    console.warn('Test - Component ngOnDestroy');
  }
}
